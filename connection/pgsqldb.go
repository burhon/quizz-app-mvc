package connection

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

const (
	DB_HOST     = "postgres://omwjmrqs:y55EEzy2iApn1quXnlwjNrH7wVvMOfpy@chunee.db.elephantsql.com/omwjmrqs"
	DB_USER     = "omwjmrqs"
	DB_PASSWORD = "y55EEzy2iApn1quXnlwjNrH7wVvMOfpy"
	DB_NAME     = "omwjmrqs"
)

func SetupDB() *sql.DB {
	dbinfo := fmt.Sprintf("host=%s port=5432 user=%s password=%s dbname=%s sslmode=disable", DB_HOST, DB_USER, DB_PASSWORD, DB_NAME)
	db, err := sql.Open("postgres", dbinfo)
	checkErr(err)

	return db
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
