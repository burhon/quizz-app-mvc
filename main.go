package main

import (
	// pgsqldb "./connection"

	"quizz-app-mvc/routes"

	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()
	// db := pgsqldb.SetupDB()
	routes.SetupRoutes(app)
	// app.Get("/", func(c *fiber.Ctx) error {
	// 	fmt.Println("1")
	// 	sqlStatement := `
	// 	CREATE TABLE user(
	// 		id BIGSERIAL NOT NULL PRIMARY KEY,
	// 		first_name VARCHAR(100) NOT NULL,
	// 		last_name VARCHAR(100) NOT NULL
	// 	)`
	// 	_, err := db.Exec(sqlStatement)
	// 	if err == nil {
	// 		return err
	// 	}
	// 	return c.SendString("Hello, World!")
	// })

	app.Listen(":3000")
}
