package routes

import (
	"quizz-app-mvc/controllers/auth"

	"github.com/gofiber/fiber/v2"
)

func SetupRoutes(app *fiber.App) {
	v1 := app.Group("/v1")

	SetupAuth(v1)
}

func SetupAuth(grp fiber.Router) {
	Login := auth.Login

	authRoutes := grp.Group("/auth")

	authRoutes.Get("/login", Login)
}
